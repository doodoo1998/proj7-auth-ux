from itsdangerous import (TimedJSONWebSignatureSerializer \
                          as Serializer, BadSignature, \
                          SignatureExpired)
import time

# initialization
# app = Flask(__name__)
# app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'

def generate_auth_token(expiration,id):
    # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
    s = Serializer('KKKK', expires_in=expiration)
    # pass index of user
    return s.dumps({'id': id})

def verify_auth_token(token):
    s = Serializer('KKKK')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"
